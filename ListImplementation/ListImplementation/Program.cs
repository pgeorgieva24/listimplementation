﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> firstList = new MyList<int>();
            firstList.Add(1);
            firstList.Add(2);
            firstList.Add(3);
            firstList.Add(4);
            firstList.Add(5);
            firstList.Add(6);
            MyList<int> listToAdd = new MyList<int>();
            listToAdd.Add(7);
            listToAdd.Add(8);
            listToAdd.Add(9);
            listToAdd.Add(10);
            
            Console.WriteLine();
            firstList.AddRange(listToAdd);
            
            //firstList.RemoveAt(10);
            for (int i = 0; i < firstList.Count; i++)
            {
                Console.Write(firstList[i] + ", ");
            }
            Console.WriteLine();
            Console.WriteLine(firstList.Contains(4));

            Console.WriteLine();

            //List<int> listotint = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 4, 4, 4, };
            //List<double> nothing = new List<double>() { 77};
            //Console.WriteLine(listotint.Capacity + " capacity");
            //Console.WriteLine(listotint.IndexOf(77));
            //foreach (var item in listotint)
            //{
            //    Console.Write(item + " ,");
            //}
            //Console.WriteLine(listotint[4]);
            //Console.WriteLine(listotint.Capacity + " capacity");

            Console.ReadLine();
        }
    }
}
