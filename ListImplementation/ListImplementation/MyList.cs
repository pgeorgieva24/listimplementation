﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListImplementation
{
    public class MyList<T> : ICustomList<T>
    {
        T[] MyArray = new T[0];
        int count = 0;
        public int Count { get => count; }

        public T this[int index]
        {
            get
            {
                if (index >= count || index < 0)
                {
                    throw new ArgumentOutOfRangeException("Index is outside the boundaries of the array.");
                }
                else return MyArray[index];
            }
            set
            {
                if (index >= count || index < 0)
                {
                    throw new ArgumentOutOfRangeException("Index is outside the boundaries of the array.");
                }
                else
                {
                    MyArray[index] = value;
                }
            }
        }

        void ResizeArray()
        {
            if (MyArray.Length == 0)
            {
                Array.Resize<T>(ref MyArray, 4);
            }
            else
            {
                int size = MyArray.Length * 2;
                Array.Resize<T>(ref MyArray, size);
            }
        }

        public void Add(T item)
        {
            count++;
            if (count > MyArray.Length)
            {
                ResizeArray();
            }
            MyArray[count - 1] = item;
        }

        public bool Remove(T item)
        {
            for (int i = 0; i < count; i++)
            {
                if (MyArray[i].Equals(item))
                {
                    for (int j = i; j < count - 1; j++)
                    {
                        MyArray[j] = MyArray[j + 1];
                    }
                    count--;
                    return true;
                }
            }
            return false;
        }

        public void Insert(int index, T item)
        {
            if (index > count || index < 0)
            {
                throw new ArgumentOutOfRangeException("Index is outside the boundaries of the array.");
            }
            else
            {
                count++;
                if (count > MyArray.Length)
                {
                    ResizeArray();
                }

                for (int i = count - 1; i >= index; i--)
                {
                    MyArray[i] = MyArray[i - 1];
                    if (i == index)
                    {
                        MyArray[index] = item;
                    }
                }
            }
        }

        public void RemoveAt(int index)
        {
            if (index >= count || index < 0)
            {
                throw new ArgumentOutOfRangeException("Index was out of range. Must be non-negative and less than the size of the collection.");
            }
            else
            {
                for (int j = index; j < count; j++)
                {
                    MyArray[j] = MyArray[j + 1];
                }
                count--;
            }
        }

        public void AddRange(MyList<T> listToAdd)
        {

            int sizeOfListToAdd = listToAdd.Count;
            while (MyArray.Length < count + sizeOfListToAdd)
            {
                ResizeArray();
            }
            int j = 0;
            for (int i = count; i < (count + sizeOfListToAdd); i++)
            {

                MyArray[i] = listToAdd[j];
                j++;
            }
            count += sizeOfListToAdd;

        }

        public void Clear()
        {
            Array.Clear(MyArray, 0, count);
            count = 0;           
        }

        public bool Contains(T item)
        {

            return IndexOf(item) >= 0;
        }

        public int IndexOf(T item)
        {
            int index = -1;
            for (int i = 0; i < count; i++)
            {
                if (MyArray[i].Equals(item))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public void Reverse()
        {
            T[] tempArray = new T[MyArray.Length];
            Array.Copy(MyArray, tempArray, MyArray.Length);
            int tempTransferIndex=count - 1;

            for (int i = 0; i < count;i++)
            {
                MyArray[i]=tempArray[tempTransferIndex];
                tempTransferIndex--;
            }
        }


    }
}
