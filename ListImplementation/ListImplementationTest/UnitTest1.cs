﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ListImplementation;

namespace ListImplementationTest
{
    [TestClass]
    public class ListIndexOfTests
    {
        [TestMethod]
        public void IndexOf_WithExistingItem_Returns_ItemIndex()
        {
            MyList<int> testList = new MyList<int>();
            testList.Add(3);
            testList.Add(4);
            testList.Add(5);
            testList.Add(6);
            testList.IndexOf(5);
            int expected = 2;
            int actual = testList.IndexOf(5);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IndexOf_WithNotExistingItem_Returns_Minus1()
        {
            MyList<int> testList = new MyList<int>();
            testList.Add(3);
            testList.Add(4);
            testList.Add(5);
            testList.Add(6);
            int expected = -1;
            int actual = testList.IndexOf(55);
            Assert.AreEqual(expected, actual);
        }
    }
}
